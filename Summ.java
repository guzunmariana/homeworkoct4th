public class Summ {
    public static void main(String[] args) {
        int i = 0;
        int sum = 0;
        int n = 6;
        while (i < n) {
            sum += i;
            i++;
        }
        System.out.println("The Sum is " + sum);
    }
}
